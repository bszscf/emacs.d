(add-to-list 'load-path "~/.emacs.d/plugins/yasnippet/yasnippet")
(require 'yasnippet)
(require 'yasnippet-snippets)
(setq yas-snippet-dirs
      '("~/.emacs.d/snippets" ;; personal snippets
        "~/.emacs.d/snippets/from-github/yasnippet-snippets/snippets"
        ))

(yas-global-mode t)

(provide 'init-yasnippet)
