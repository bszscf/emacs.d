(add-to-list 'load-path "~/.emacs.d/elpa-27.1/lsp-mode-20200819.1349")
(add-to-list 'load-path "~/.emacs.d/plugins/lsp/javascript-lsp")


(add-hook 'js-mode-hook #'lsp-mode)
(add-hook 'typescript-mode-hook #'lsp-mode) ;; for typescript support
(add-hook 'js3-mode-hook #'lsp-mode) ;; for js3-mode support
(add-hook 'rjsx-mode #'lsp-mode) ;; for rjsx-mode support

(provide 'init-lsp-javascript)
