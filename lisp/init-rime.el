(add-to-list 'load-path "~/.emacs.d/plugins/emacs-rime")
(require 'rime)
(require 'posframe)

;;; Code:
(setq rime-user-data-dir "~/.config/ibus/rime")

(setq rime-posframe-properties
      (list :background-color "#333333"
            :foreground-color "#dcdccc"
            :font "WenQuanYi Micro Hei Mono-14"
            :internal-border-width 10))

(setq default-input-method "rime"
      rime-show-candidate 'posframe)

(provide 'init-rime)
