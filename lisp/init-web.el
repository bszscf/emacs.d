(require 'web-mode)
;;(require 'js-auto-beautify)
;;(require 'editorconfig)

;;(editorconfig-mode 1)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))

;;(add-hook 'web-mode-hook 'js-auto-beautify-mode)

(setq
 web-mode-style-padding 0
 web-mode-script-padding 0
 web-mode-block-padding 0
 web-mode-enable-current-element-highlight t
 web-mode-enable-auto-indentation nil
 web-mode-comment-formats '(("java" . "//") ("javascript" . "//") ("php" . "//")))
(setq-default
 web-mode-markup-indent-offset 2
 web-mode-code-indent-offset 2
 web-mode-css-indent-offset 2)

(modify-syntax-entry ?' "\"" web-mode-syntax-table)
(modify-syntax-entry ?` "\"" web-mode-syntax-table)
;; "-" as word so company completes kabeb-case
(modify-syntax-entry ?_ "w" web-mode-syntax-table)
(modify-syntax-entry ?- "w" web-mode-syntax-table)
(modify-syntax-entry ?# "_" web-mode-syntax-table)

(provide 'init-web)
